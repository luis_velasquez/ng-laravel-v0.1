<!doctype html>
<html lang="en" ng-app="commentApp">
<head>
    <meta charset="UTF-8">
    <title>Angular Demo</title>
    <script src="js/angular.min.js"></script>
    <script src="js/angular-route.min.js"></script>
    <script src="js/app.js"></script>
    <script src="js/controllers/list.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="main" ng-view></div>
</body>
</html>