/**
 * Created by Edward on 12/2/2014.
 */
// public/js/services/commentService.js
angular.module('pageService', [])

    .factory('Page', function($http) {

        return {
            // get all the comments
            get : function() {
                return $http.get('/api/pages');
            },

            // save a comment (pass in comment data)
            save : function(commentData) {
                return $http({
                    method: 'POST',
                    url: '/api/pages',
                    headers: { 'Content-Type' : 'application/x-www-form-urlencoded' },
                    data: $.param(commentData)
                });
            },

            // destroy a comment
            destroy : function(id) {
                return $http.delete('/api/pages/' + id);
            }
        }

    });