// public/js/app.js
var commentApp = angular.module('commentApp', ['mainCtrl', 'commentService','ngRoute']);

commentApp.config(['$routeProvider','$locationProvider', function($routeProvider,$locationProvider) {
    $routeProvider.
        when('/comments', {
            templateUrl: 'js/partials/cms.html',
            controller: 'mainController'
        }).
        when('/testing', {
            templateUrl:'js/partials/testing.html',
            controller: 'mainController'
        }).
        otherwise({
            redirectTo: '/'
        });

    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });
}]);
	
	